namespace Webjet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialcommit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        username = c.String(nullable: false),
                        password = c.String(nullable: false),
                        server = c.String(nullable: false),
                        serverIp = c.String(nullable: false),
                        dbUserID = c.String(),
                        dbPassword = c.String(),
                        dbInitialCatalog = c.String(),
                        dbDataSource = c.String(),
                        createdAt = c.DateTime(nullable: false),
                        modifiedAt = c.DateTime(nullable: false),
                        admin1 = c.String(),
                        admin2 = c.String(),
                        createdBy_Id = c.Int(),
                        modifiedBy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserPersonals", t => t.createdBy_Id)
                .ForeignKey("dbo.UserPersonals", t => t.modifiedBy_Id)
                .Index(t => t.createdBy_Id)
                .Index(t => t.modifiedBy_Id);
            
            CreateTable(
                "dbo.UserPersonals",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        email = c.String(),
                        password = c.String(),
                        firstName = c.String(),
                        lastName = c.String(),
                        phone = c.String(),
                        address = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProjects", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.UserProjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        employeeId = c.String(),
                        designation = c.String(),
                        createdAt = c.DateTime(nullable: false),
                        modifiedAt = c.DateTime(nullable: false),
                        createdBy_Id = c.Int(),
                        manager_Id = c.Int(),
                        modifiedBy_Id = c.Int(),
                        UserStatus_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserPersonals", t => t.createdBy_Id)
                .ForeignKey("dbo.UserPersonals", t => t.manager_Id)
                .ForeignKey("dbo.UserPersonals", t => t.modifiedBy_Id)
                .ForeignKey("dbo.UserStatus", t => t.UserStatus_Id)
                .Index(t => t.createdBy_Id)
                .Index(t => t.manager_Id)
                .Index(t => t.modifiedBy_Id)
                .Index(t => t.UserStatus_Id);
            
            CreateTable(
                "dbo.UserStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        statusName = c.String(),
                        statusColor = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        contract = c.String(),
                        project = c.String(),
                        desc = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        contract = c.String(),
                        desc = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        roleName = c.String(),
                        priority = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        staffcode = c.String(),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        statusName = c.String(),
                        statusColor = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserTokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TokenDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        AccessToken = c.String(nullable: false),
                        IpAddress = c.String(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserPersonals", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTokens", "User_Id", "dbo.UserPersonals");
            DropForeignKey("dbo.Admins", "modifiedBy_Id", "dbo.UserPersonals");
            DropForeignKey("dbo.Admins", "createdBy_Id", "dbo.UserPersonals");
            DropForeignKey("dbo.UserPersonals", "Id", "dbo.UserProjects");
            DropForeignKey("dbo.UserProjects", "UserStatus_Id", "dbo.UserStatus");
            DropForeignKey("dbo.UserProjects", "modifiedBy_Id", "dbo.UserPersonals");
            DropForeignKey("dbo.UserProjects", "manager_Id", "dbo.UserPersonals");
            DropForeignKey("dbo.UserProjects", "createdBy_Id", "dbo.UserPersonals");
            DropIndex("dbo.UserTokens", new[] { "User_Id" });
            DropIndex("dbo.UserProjects", new[] { "UserStatus_Id" });
            DropIndex("dbo.UserProjects", new[] { "modifiedBy_Id" });
            DropIndex("dbo.UserProjects", new[] { "manager_Id" });
            DropIndex("dbo.UserProjects", new[] { "createdBy_Id" });
            DropIndex("dbo.UserPersonals", new[] { "Id" });
            DropIndex("dbo.Admins", new[] { "modifiedBy_Id" });
            DropIndex("dbo.Admins", new[] { "createdBy_Id" });
            DropTable("dbo.UserTokens");
            DropTable("dbo.Status");
            DropTable("dbo.Staffs");
            DropTable("dbo.Roles");
            DropTable("dbo.ProjectNames");
            DropTable("dbo.Projects");
            DropTable("dbo.UserStatus");
            DropTable("dbo.UserProjects");
            DropTable("dbo.UserPersonals");
            DropTable("dbo.Admins");
        }
    }
}
