﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Webjet.Migrations;

namespace Webjet
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
           // Database.SetInitializer(new DbInitializer());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MyDBContext, Configuration>());
            MyDBContext db = new MyDBContext();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            //Database.SetInitializer(new DatabaseInitializer());
            AreaRegistration.RegisterAllAreas();
        }
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            string url = Request.Url.LocalPath;
            if (!System.IO.File.Exists(Context.Server.MapPath(url)) && !url.Contains("/api/"))
                    Context.RewritePath("/View/app/index.html");
        }

    }
}
