﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Webjet.Controllers;
using Webjet.HelperClass;
using Webjet.Models;

namespace Webjet.Test
{
    [TestClass]
    public class MovieTest
    {
        public static string movieId = "tt2488496"; //from imdb
       
        [TestMethod]
        public void getAllMoviesTest()
        {
           // Set up Prerequisites   
           var controller = new MovieController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act on Test  
            var response = controller.Get();
            var contentResult = response as OkNegotiatedContentResult<IEnumerable<Movie>>;
            // Assert the result  
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

        }
        [TestMethod]
        public void getMovieByIdTest()
        {
            // Set up Prerequisites   
            var controller = new MovieController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act on Test  
            var response = controller.Get(movieId);
            var contentResult = response as OkNegotiatedContentResult<MovieDetails>;
            // Assert the result  
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

        }
        [TestMethod]
        public void getMoviePricesTest()
        {
            // Set up Prerequisites   
            var controller = new MovieController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act on Test  
            var response = controller.Put(movieId);
            var contentResult = response as OkNegotiatedContentResult<List<MoviePrice>>;
            // Assert the result  
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

        }
    }
}