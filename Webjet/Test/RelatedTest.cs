﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Webjet.Controllers;
using Webjet.Models;

namespace Webjet.Test
{
    [TestClass]
    public class RelatedTest
    {
        [TestMethod]
        public void getFromImdbTest()
        {
            // Set up Prerequisites   
            var controller = new RelatedController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act on Test  
            var response = controller.Post(new Movie { Title="Start Wars"});
            var contentResult = response as OkNegotiatedContentResult<IEnumerable<Imdb>>;
            // Assert the result  
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

        }
    }
}