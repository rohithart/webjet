﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webjet.HelperClass;
using Webjet.Models;

namespace Webjet.Controllers
{
    public class RelatedController : ApiController
    {
        // GET: api/Related/5
        public IHttpActionResult Post([FromBody]Movie movie)
        {
            return Ok(getFromImdb(movie.Title));
        }


        private IEnumerable<Imdb> getFromImdb(string title)
        {
            string url = "http://www.imdb.com/xml/find?json=1&nr=1&tt=on&q=";
            return RelatedHelper.getFromImdbServer(url + title);
        }

    }
}
