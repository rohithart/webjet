﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webjet.HelperClass;
using Webjet.Models;

namespace Webjet.Controllers
{
    public class MovieController : ApiController
    {
        // GET: api/Movie
        public IHttpActionResult Get()
        {
            return Ok(getAllMovies());
        }
        // GET: api/Movie/1
        public IHttpActionResult Get(string id)
        {
            return Ok(getMovieById(id));
        }


        // PUT: api/Movie/1
        public IHttpActionResult Put(string id)
        {
            return Ok(getMoviePrices(id));
        }

        private IEnumerable<Movie> getAllMovies()
        {
            IEnumerable<Movie> movieList = null;

            Source source = new Source();
            foreach (var services in source.sourceList)
            {
                movieList = services.getAllMovies();
                if(movieList!= null)
                    if (movieList.Count() != 0)
                        break;
            }
            return movieList;
        }
        private MovieDetails getMovieById(string id)
        {
            MovieDetails detail = null;
            Source source = new Source();
            foreach (var services in source.sourceList)
            {
                detail = services.getMovieById(id);
                if (detail != null)
                    break;
            }
            return detail;
        }
        private List<MoviePrice> getMoviePrices(string id)
        {
            List<MoviePrice> priceList = new List<MoviePrice>();
            Source source = new Source();
            foreach (var services in source.sourceList)
            {
                string price = services.getMoviePrices(id);
                if (price != null)
                {
                    priceList.Add(new MoviePrice { Price = price, Source = services.source });
                }
                else
                {
                    priceList.Add(new MoviePrice { Price = "Not Found", Source = services.source });
                }
            }
            return priceList.OrderBy(c => c.Price.Length).ThenBy(c => c.Price).ToList();
        }
    }
}
