(function () {
    'use strict';

    angular.module('Webjet').controller('SigninCtrl', signinController);
    signinController.$inject = ['$rootScope', '$scope', '$location', '$cookies', 'toaster', 'signinService'];
    function signinController($rootScope, $scope, $location, $cookies, toaster, signinService) {
        var signinVM = this;
        signinVM.userLoginData = {email: '', password: '', repeatPassword: ''};
        signinVM.message = 'hello';
        signinVM.email = sessionStorage.getItem('email');
        signinVM.empid = sessionStorage.getItem('empid');
        signinVM.id = sessionStorage.getItem('id');
        signinVM.designation = sessionStorage.getItem('designation');
        signinVM.roles = JSON.parse(sessionStorage.getItem('roles'));
        signinVM.departments = JSON.parse(sessionStorage.getItem('departments'));
        signinVM.loginUser = {};
        signinVM.adminFlag = false;
        signinVM.managementFlag = false;
        signinVM.departmentFlag = false;
        signinVM.isLogginClicked = false;

        $rootScope.$watch(
            // This function returns the value being watched. It is called for each turn of the $digest loop
            function () {
                return signinService.email;
            },
            // This is the change listener, called when the value returned from the above function changes
            function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    signinVM.email = signinService.email;
                    signinVM.roles = signinService.roles;
                    signinVM.departments = signinService.departments;
                }
            }
        );

        //login to server
        signinVM.login = function () {
            //var  a =signinVM.userLoginData.email.indexOf("@citysoft.net.au");
            //toaster.pop({type: 'error', title: "Error", body: "Invalid email! "+a, showCloseButton: true});
            signinVM.isLogginClicked = true;
            if (signinVM.userLoginData.email.indexOf("@citysoft.net.au") == '-1') {
                toaster.pop({type: 'error', title: "Error", body: "Invalid email!", showCloseButton: true});
            }
            else {
                signinService.login(signinVM.userLoginData).then(function (response) {
                    if (response === "No License") {
                        toaster.pop({
                            type: 'error',
                            title: "Error",
                            body: "Invalid License!",
                            showCloseButton: true
                        });
                        $location.path('/license')
                    }
                    else {
                        if (response != null) {
                            signinVM.userLoginData = {};
                            signinVM.loginUser = response;
                            signinVM.email = signinService.email;
                            signinVM.id = signinService.id;
                            signinVM.empid = signinService.empid;
                            signinVM.designation = signinService.designation;
                            signinVM.roles = signinService.roles;
                            signinVM.departments = signinService.departments;
                            toaster.pop({
                                type: 'success',
                                title: "Login",
                                body: "Successfully Logged in",
                                showCloseButton: true
                            });
                            $location.path('/account')
                            signinVM.initUser();
                        }
                        else {
                            signinService.email = '';
                            signinService.roles = [];
                            signinService.departments = [];
                            toaster.pop({
                                type: 'error',
                                title: "Error",
                                body: "Invalid credentials!",
                                showCloseButton: true
                            });
                        }
                    }

                });
            }
        }
        signinVM.initUser = function () {
            if (signinVM.email) {
                for (var i = 0; i < signinVM.roles.length; i++) {
                    if (signinVM.roles[i].priority == 0 && signinVM.departments[i].departmentName === 'Admin') {
                        signinVM.adminFlag = true;
                        signinVM.managementFlag = true;
                    }

                    if (signinVM.roles[i].priority == 0 && signinVM.departments[i].departmentName === 'Management')
                        signinVM.managementFlag = true;
                    if (signinVM.roles[i].priority < 3)
                        signinVM.departmentFlag = true;
                }
                $rootScope.$emit("Update", 'aaa');
            }
        }
        signinVM.register = function () {
            var errorFlag = true;
            var errorMessage = 'Unexpected error!!'
            if (signinVM.userLoginData.email.indexOf("@citysoft.net.au") == '-1') {
                errorFlag = false;
                errorMessage = 'Not a valid email';
            }
            if (signinVM.userLoginData.password != signinVM.userLoginData.repeatPassword) {
                errorFlag = false;
                errorMessage = 'Password not matching';
            }
            if (!signinVM.userLoginData.firstName || !signinVM.userLoginData.lastName || !signinVM.userLoginData.phone || !signinVM.userLoginData.address) {
                errorFlag = false;
                errorMessage = 'empty Fields';
            }
            if (errorFlag === true) {
                signinService.register(signinVM.userLoginData).then(function (response) {

                    if (response === true) {
                        toaster.pop({
                            type: 'success',
                            title: "Awating Approval",
                            body: "Successfully Registered. Admin Approval required for login",
                            showCloseButton: true
                        });
                        signinVM.userLoginData = {};
                        $location.path('/')
                    }
                    else {
                        toaster.pop({
                            type: 'error',
                            title: "Error",
                            body: "Error registering new user",
                            showCloseButton: true
                        });
                    }

                });
            }
            else {
                toaster.pop({
                    type: 'error',
                    title: "Error",
                    body: errorMessage,
                    showCloseButton: true
                });
            }
        }
        signinVM.userLogout = function () {
            signinVM.email = '';
            signinVM.empid = '';
            signinVM.designation = '';
            signinService.email = '';
            signinService.empid = '';
            signinService.designation = '';
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('id');
            sessionStorage.removeItem('empid');
            sessionStorage.removeItem('designation');
            sessionStorage.removeItem('Authorization');
            $cookies.remove("Authorization");
            $cookies.remove("user");
            signinVM.adminFlag = false;
            signinVM.managementFlag = false;
            signinVM.departmentFlag = false;
            signinVM.isLogginClicked = false;
            signinVM.userLoginData = {};
            signinVM.loginUser = {};
            toaster.pop({
                type: 'success',
                title: "Login",
                body: "Successfully Logged out",
                showCloseButton: true
            });
            $location.path('/')

        }
        signinVM.init = function () {
            //signinVM.getOrder();
            signinVM.adminFlag = false;
            signinVM.managementFlag = false;
            signinVM.departmentFlag = false;
            signinVM.isLogginClicked = false;
            signinVM.initUser();

        };
        signinVM.init();
    };

})();
