/**
 * Created by Administrator on 04/12/2015.
 */
(function () {
    'use strict';
    angular.module('Webjet').service('signinService', signinService);
    signinService.$inject = ['$q', '$http','$cookies'];
    function signinService($q, $http, $cookies) {
        var self = this;

        self.email;
        self.roles=[];
        self.departments=[];

        self.register = function (loginData) {
            loginData.statusId = 2;
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: '/api/Users',
                data: (loginData)
            })
                .success(function (data) {
                    //self.username = loginData.username;
                    //self.server = loginData.server;
                    //sessionStorage.setItem('username', loginData.username);
                    //sessionStorage.setItem('server', loginData.server);
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                });

            return defer.promise;
        };

        self.login = function (userLoginData) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: '/api/Login',
                //config.headers['Token']:  sessionStorage.token,
                data: (userLoginData)
            })
                .success(function (data, headers) {
                    if(data != null && data != 'No License') {

                        self.email = data.email;
                        self.id = data.Id;
                        self.empid = data.UserProject.employeeId;
                        self.designation = data.UserProject.designation;
                        angular.forEach(data.UserProject.UserDepartments, function (item) {
                            self.departments.push(item.Department);
                            self.roles.push(item.Role);
                        });
                        sessionStorage.setItem("roles", JSON.stringify(self.roles));
                        sessionStorage.setItem("departments", JSON.stringify(self.departments));
                        sessionStorage.setItem('email', self.email);
                        sessionStorage.setItem('id', self.id);
                        sessionStorage.setItem('designation', self.designation);
                        sessionStorage.setItem('empid', self.empid);
                    }
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
                .then(function(response) {
                    var data = response.data;
                    var header = response.headers;
                    $cookies.put('Authorization',header('Authorization'));
                    $cookies.put('user',sessionStorage.getItem('id'));
                    sessionStorage.setItem('Authorization', header('Authorization'));
                    $http.defaults.headers.common['Authorization'] = sessionStorage.getItem('Authorization');
                    $http.defaults.headers.common['user'] = sessionStorage.getItem('email');

                });

            return defer.promise;
        };


    };

})();
