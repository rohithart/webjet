(function () {
    'use strict';

    angular.module('Webjet').controller('MovieCtrl', movieController);
    movieController.$inject = ['$location', '$sce','$http','toaster', 'movieService'];
    function movieController($location, $sce,$http, toaster, movieService) {
        var movieVM = this;
        movieVM.movies = '';
        movieVM.movie = '';
        movieVM.searchText = '';

        movieVM.viewDetails = function (movieNumber) {
            movieVM.movies.forEach(function (r) {
                if (r.ID === movieNumber) {
                    movieVM.movie = r;
                    movieVM.editFlag = true;
                    movieVM.editTabFlag = true;
                }
            });
            movieService.getMovieDetails(movieVM.movie.ID).then(function (response) {
                if (response != '' && response != null)
                {
                    movieVM.movie = response;
                    movieVM.movie.genreList = movieVM.movie.Genre.split(',');
                }
            });
            movieService.getMoviePrices(movieVM.movie.ID).then(function (response) {
                if (response != '' && response != null) {
                    movieVM.moviePrice = response;
                }
            });
            movieService.getFromImdb(movieVM.movie).then(function (response) {
                if (response != '' && response != null) {
                    movieVM.relatedImdb = response;
                    movieVM.relatedImdb.forEach(function (r) {
                        if (r.title_description.indexOf("<a href='") >= 0)
                            r.title_description=r.title_description.replace("<a href='", "<a href='http://www.imdb.com");
                    });
                }
            });
        };
        movieVM.getMovies = function () {
            movieService.getMovies().then(function (response) {
                movieVM.movies = response;
            });
        };
        movieVM.reset = function () {
            movieVM.editFlag = false;
            movieVM.editTabFlag = false;
            movieVM.movie = {};

        };
        movieVM.init = function () {
            movieVM.getMovies();
            movieVM.editFlag = false;
            movieVM.editTabFlag = false;
        };
        movieVM.init();

    };

})();
