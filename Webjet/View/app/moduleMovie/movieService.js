/**
 * Created by Administrator on 04/12/2015.
 */
(function () {
    'use strict';
    angular.module('Webjet').service('movieService', movieService);
    movieService.$inject = ['$q', '$http'];
    function movieService($q, $http) {
        var self = this;
        if (sessionStorage.getItem('Authorization')) {
            $http.defaults.headers.common['Authorization'] = sessionStorage.getItem('Authorization');
            $http.defaults.headers.common['user'] = sessionStorage.getItem('email');
        }
        self.getMovies = function (){
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: '/api/Movie'

            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
            ;
            return defer.promise;
        };

        self.getMovieDetails = function (id) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: '/api/Movie/'+id,

            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
            ;
            return defer.promise;
        };
        self.getMoviePrices = function (id) {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                url: '/api/Movie/' + id,

            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
            ;
            return defer.promise;
        };
        self.getFromImdb = function (movie) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: '/api/Related/',
                data: movie
            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
            ;
            return defer.promise;
        };

    };

})();
