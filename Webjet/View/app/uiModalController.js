/**
 * Created by Administrator on 05/01/2016.
 */
(function () {
    'use strict';

    angular.module('Webjet').controller('ModalCtrl', modalController);
    modalController.$inject = ['$uibModalInstance', 'details'];
    function modalController($uibModalInstance, details) {
        var modalVM = this;
        modalVM.details = details;

        modalVM.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();