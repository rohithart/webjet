﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using Webjet.Models;

namespace Webjet.HelperClass
{
    public class MovieHelper
    {
        MyDBContext context = new MyDBContext();

        public static IEnumerable<Movie> getAllMoviesFromServer(string url)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add(XMLHelper.getTokenName(), XMLHelper.getTokenValue());
            HttpResponseMessage response = null;
            response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseValue = response.Content.ReadAsStringAsync().Result;
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<MovieList>(responseValue);
                return list.Movies;
            }
            return null;
        }

        public static MovieDetails getMovieByIdsFromServer(string url)
        {
            MovieDetails movie = new MovieDetails();
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add(XMLHelper.getTokenName(), XMLHelper.getTokenValue());
            HttpResponseMessage response = null;
            response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseValue = response.Content.ReadAsStringAsync().Result;
                movie = Newtonsoft.Json.JsonConvert.DeserializeObject<MovieDetails>(responseValue);
                return movie;
            }
            return null;
        }
        public static string getMoviePriceFromServer(string url)
        {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add(XMLHelper.getTokenName(), XMLHelper.getTokenValue());
            HttpResponseMessage response = null;
            response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseValue = response.Content.ReadAsStringAsync().Result;
                return Newtonsoft.Json.JsonConvert.DeserializeObject<MovieDetails>(responseValue).Price;
            }
            return null;
        }


    }
}