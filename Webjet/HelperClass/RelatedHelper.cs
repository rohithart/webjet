﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using Webjet.Models;

namespace Webjet.HelperClass
{
    public class RelatedHelper
    {
        public static IEnumerable<Imdb> getFromImdbServer(string url)
        {
            HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Add(XMLHelper.getTokenName(), XMLHelper.getTokenValue());
            HttpResponseMessage response = null;
            response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseValue = response.Content.ReadAsStringAsync().Result;
                JObject json = JObject.Parse(responseValue);
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<ImdbList>(json.ToString());
                return list.title_approx;
            }
            return null;
        }
    }
}