﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml.Linq;

namespace Webjet.HelperClass
{
    public class XMLHelper
    {
        public static string fileName;


        int i = 0;
        public static string getTokenName()
        {
            try {
                fileName = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data/webjet.lic");

            }
            catch (Exception e)
            {
                fileName = "C:\\Users\\Rohith\\Documents\\Webjet\\Webjet\\App_Data\\webjet.lic";

            }

            string token = "";
            if (File.Exists(fileName))
            {
                XDocument doc = XDocument.Load(fileName);
                foreach (XElement el in doc.Root.Elements())
                {
                    if (el.Name == "token")
                    {
                        foreach (XElement element in el.Elements())
                        {
                            if (element.Name == "name")
                            {
                                token = element.Value;
                                return token;
                            }
                        }
                    }
                }
            }
            return null;
        }


        public static string getTokenValue()
        {
            string token = "";
            if (File.Exists(fileName))
            {
                XDocument doc = XDocument.Load(fileName);
                foreach (XElement el in doc.Root.Elements())
                {
                    if (el.Name == "token")
                    {
                        foreach (XElement element in el.Elements())
                        {
                            if (element.Name == "value")
                            {
                                token = element.Value;
                                return token;
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}