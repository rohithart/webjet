﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webjet.Models
{
    public class MoviePrice
    {
        public string Source { get; set; }
        public string Price { get; set; }
    }
}