﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Webjet.HelperClass;

namespace Webjet.Models
{
    public class Movie
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }
        public string Type { get; set; }
        public string Poster { get; set; }

        [NotMapped]
        public string source { get; set; }

        [NotMapped]
        private string identifier { get; set; }
        [NotMapped]
        private string getByIdUrl { get; set; }
        [NotMapped]
        private string getAllUrl { get; set; }
        [NotMapped]
        public static string baseUrl = "http://webjetapitest.azurewebsites.net/api/";
        public void setIdentifier(string identifier)
        {
            this.identifier = identifier;
        }
        public void setIdUrl(string url)
        {
            this.getByIdUrl = url;
        }
        public void setAllUrl(string url)
        {
            this.getAllUrl = url;
        }

        public IEnumerable<Movie> getAllMovies()
        {
            string url = this.getAllUrl;
            return MovieHelper.getAllMoviesFromServer(url);
        }
        public MovieDetails getMovieById(string fullId)
        {
            string id = fullId.Remove(0, 2);
            string url = this.getByIdUrl;
            string prefix = this.identifier;
            return MovieHelper.getMovieByIdsFromServer(url + prefix +id);
        }
        public string getMoviePrices(string fullId)
        {
            string id = fullId.Remove(0, 2);
            string url = this.getByIdUrl;
            string prefix = this.identifier;
            return MovieHelper.getMoviePriceFromServer(url + prefix + id);
        }
    }
}