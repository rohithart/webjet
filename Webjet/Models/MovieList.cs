﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webjet.Models
{
    public class MovieList
    {
        public IEnumerable<Movie> Movies { get; set; }
    }
}