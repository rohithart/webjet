﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webjet.Models
{
    public class FilmWorld:Movie
    {
        public FilmWorld()
        {
            this.setIdentifier("fw");
            this.setAllUrl(baseUrl+"filmworld/movies/");
            this.setIdUrl(baseUrl + "filmworld/movie/");
            this.source = "Film World";
        }
    }
}