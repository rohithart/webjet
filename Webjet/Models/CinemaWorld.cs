﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webjet.Models
{
    public class CinemaWorld : Movie
    {
        public CinemaWorld()
        {
            this.setIdentifier("cw");
            this.setAllUrl(baseUrl+"cinemaworld/movies/");
            this.setIdUrl(baseUrl + "cinemaworld/movie/");
            this.source = "Cinema World";
        }
    }
}