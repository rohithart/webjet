﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webjet.Models
{
    public class Source
    {
        public List<Movie> sourceList = new List<Movie>();

        public Source() {
            sourceList.Add(new CinemaWorld { });
            sourceList.Add(new FilmWorld { });
        }
    }
}